package googlecloud

import (
	"context"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"

	"golang.org/x/crypto/ssh"
	"google.golang.org/api/compute/v1"

	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

type PrivPub interface {
	crypto.PrivateKey
	Public() crypto.PublicKey
}

func (g *InstanceGroup) ssh(ctx context.Context, info *provider.ConnectInfo, instance *compute.Instance) error {
	var key PrivPub
	var err error

	if info.Key != nil {
		priv, err := ssh.ParseRawPrivateKey(info.Key)
		if err != nil {
			return fmt.Errorf("reading private key: %w", err)
		}
		var ok bool
		key, ok = priv.(PrivPub)
		if !ok {
			return fmt.Errorf("key doesn't export PublicKey()")
		}
	} else {
		key, err = rsa.GenerateKey(rand.Reader, 4096)
		if err != nil {
			return fmt.Errorf("generating private key: %w", err)
		}

		info.Key = pem.EncodeToMemory(
			&pem.Block{
				Type:  "RSA PRIVATE KEY",
				Bytes: x509.MarshalPKCS1PrivateKey(key.(*rsa.PrivateKey)),
			},
		)
	}

	sshPubKey, err := ssh.NewPublicKey(key.Public())
	if err != nil {
		return fmt.Errorf("generating ssh public key: %w", err)
	}

	if info.Username == "" {
		// We use "fleeting" as the default username because:
		// - "root" as a default username is a poor choice as it's often not allowed by default.
		// - GCP tooling usually uses the service account/user email, which is difficult to get with the client APIs.
		info.Username = "fleeting"
	}

	addMetadataKeyValueList(instance.Metadata, "ssh-keys", info.Username+":"+string(ssh.MarshalAuthorizedKey(sshPubKey)))

	g.log.Info("updating instance metadata", "id", instance.Name)

	// update metadata
	op, err := g.client.SetInstanceMetadata(ctx, g.Project, g.Zone, instance.Name, instance.Metadata)
	if err != nil {
		return err
	}
	if err := g.wait(ctx, op); err != nil {
		return err
	}

	return nil
}
